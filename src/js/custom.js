var globalFunc = {

	init: function(){
		globalFunc.resize();
	},

	resize: function(){
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();

		// STICKY FOOTER
		var headerHeight = $('header').outerHeight();
		var footerHeight = $('footer').outerHeight();
		var footerTop = (footerHeight) * -1;
		$('footer').css({marginTop: footerTop});
		$('#main-wrapper').css({paddingBottom: footerHeight});

	},

	touch: function(){
		if (Modernizr.touch) {
			$('html').addClass('bp-touch');
		}
	},

	dropDownMenu: function(){
		if(!Modernizr.touch) {
			$('.header__menu > ul > li').mouseenter(function(){
				$(this).children().eq(1).stop(true, false).slideDown(200);
			}).mouseleave(function(){
				$(this).children().eq(1).stop(true, false).slideUp(200);
			});
		} else {
			$('.header__menu > ul > li').click(function(){
				$(this).children().eq(1).stop(true, false).slideToggle(200);
			});
		}
	},

	scroll: function(){
		var st = $('body, html').scrollTop();
		if (st > 50){
			$('header').addClass('sm');
		} else {
			$('header').removeClass('sm');
		}
	},

	animated: function(){
		if(!Modernizr.touch){
			$('.fadeUp').appear(function(){
				var elem = $(this);
				var el = elem.find('.overflow > *');
				setTimeout(function(){
					TweenMax.set($(elem), {opacity: 1});
					TweenMax.set($(el), {opacity: 0, yPercent: 50});
					TweenMax.staggerTo($(el), 0.7, {opacity: 1, yPercent: 0, delay: 0.2, ease: Quart.easeOut }, 0.2);
				}, 100);
			});
		}
	},

	scrollNav: function(){
		var head = $('.header').height();
		$('nav a').each(function(){
			var _this = $(this);
			_this.click(function(){
				var element = _this.attr('href');
				$('html,body').animate({ scrollTop: $(element).offset().top - head}, 700);
				$('nav a').removeClass('active');
				_this.addClass('active');
				event.preventDefault();
			});
		});
	},

	burgerMenu: function(){
		$('.burger-menu').click(function(){
			$(this).toggleClass('active');
			$('.header__menu').toggleClass('open');
			$('.cover').fadeToggle('fast');
		});
	},

	masonry: function(){
		$('.grid').masonry({
			percentPosition: true,
			itemSelector: '.grid-item',
			isAnimated: true,
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
		});
	},

	magnificPopup: function(){
		$('.grid__inner .img-popup').magnificPopup({
			type: 'image'
		});

		// Video Popup
		$('.grid__inner .video-popup').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,

			fixedContentPos: false
		});

	},

};

$(window).scroll(function(event){
	globalFunc.scroll();
});

$(window).resize(function() {
	globalFunc.init();
	globalFunc.masonry();
});

$(document).ready(function() {
	globalFunc.touch();
	globalFunc.init();
	globalFunc.scroll();
	// globalFunc.scrollNav();
	globalFunc.burgerMenu();
	globalFunc.masonry();
	globalFunc.magnificPopup();
	globalFunc.dropDownMenu();

});

$(window).on('load', function() {
	globalFunc.init();
	globalFunc.animated();
});

// preloader once done
Pace.on('done', function() {
	// totally hide the preloader especially for IE
	setTimeout(function() {
		$('.pace-inactive').hide();
	}, 500);
});
